CREATE DATABASE db_web;

-- USE db_web;

CREATE TABLE tbl_people (

   ID INT(11) NOT NULL AUTO_INCREMENT,
   FNAME VARCHAR(255) NOT NULL,
   LNAME VARCHAR(255),
   EMAIL VARCHAR (255),
   PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE tbl_feedback (

   ID INT(11) NOT NULL AUTO_INCREMENT,
   TITLE VARCHAR (20) NOT NULL,
   CONTENT VARCHAR(255) NOT NULL,
   PID INT(11) NOT NULL,
   PRIMARY KEY (ID),
   FOREIGN KEY (PID) REFERENCES tbl_people(ID)

) AUTO_INCREMENT=1;

-- *********CREATE PEOPLE*********

INSERT INTO tbl_people (FNAME,LNAME,EMAIL) VALUES ('Jimmy', 'Hendrix', 'sample321@hotmail.co.nz');                                                 
INSERT INTO tbl_people (FNAME,LNAME,EMAIL) VALUES ('Eric', 'Clapton', 'example1658@hotmail.co.nz');
INSERT INTO tbl_people (FNAME,LNAME,EMAIL) VALUES ('Ozzy', 'Osbourne', 'new389@hotmail.co.nz');                                                  
INSERT INTO tbl_people (FNAME,LNAME,EMAIL) VALUES ('Brian', 'Johnson', 'example97@gmail.com');
INSERT INTO tbl_people (FNAME,LNAME,EMAIL) VALUES ('Freddy', 'Mercury', 'sample321@gmail.com');
INSERT INTO tbl_people (FNAME,LNAME,EMAIL) VALUES ('Mick', 'Jagger', 'new579@gmail.com');

-- SELECT * FROM tbl_people;
-- UPDATE tbl_people SET FNAME = 'Jeff' WHERE ID = 1;
-- DELETE FROM tbl_people WHERE ID = 1;


-- *********CREATE POSTS*********

INSERT INTO tbl_feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 6);
INSERT INTO tbl_feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi repellat vitae magni et exercitationem non optio dolorem qui iure similique!', 2);
INSERT INTO tbl_feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, illo.', 1);
INSERT INTO tbl_feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Excepturi, modi?', 4);
INSERT INTO tbl_feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 2);
INSERT INTO tbl_feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi repellat vitae magni et exercitationem non optio dolorem qui iure similique!', 3);

-- SELECT * FROM tbl_feedback;
-- UPDATE tbl_feedback SET TITLE = 'TITLE1' WHERE ID = 1;
-- DELETE FROM tbl_feedback WHERE ID = 5;

-- DROP TABLE tbl_feedback;



