<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Contact Page</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="image/logo2.png" alt="logo">Ethereal Beauty</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="signup.php">Sign Up</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

    <!-- Page Content -->
    <div class="container">
          <h1 class="mt-4 mb-3">Contact</h1>

          <!-- Content Row -->
          <div class="row">
            <!-- Map Column -->
            <div class="col-lg-8 mb-4">
              <!-- Embedded Google Map -->
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1578.6818316245574!2d176.16438425221747!3d-37.68765758169549!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d6ddbdcdb018c15%3A0x760220300fbef4de!2sUniversity+of+Waikato+-+Bongard+Campus!5e0!3m2!1sen!2snz!4v1522914577526" width="700" height="432" frameborder="0" style="border:0" allowfullscreen></iframe>            </div>
            <!-- Contact Details Column -->
            <div class="col-lg-4 mb-4">
              <h3>Contact Details</h3>
              <p>
                1234 Example Street
                <br>Tauranga, New Zealand 3112
                <br>
              </p>
              <p>
                <abbr title="Phone">P</abbr>: (123) 456-7890
              </p>
              <p>
                <abbr title="Email">E</abbr>:
                <a href="mailto:name@example.com">name@example.com
                </a>
              </p>
              <p>
                <abbr title="Hours">H</abbr>: Monday - Friday: 9:00 AM to 3:00 PM
              </p>
              <!-- Embedded Weather Widget -->
              <div id="metservice-widget">
              <script>
              (function(d){
              var i = d.createElement("iframe");
              i.setAttribute("src", "https://services.metservice.com/weather-widget/widget?params=blue|medium|portrait|days-3|modern&loc=tauranga&type=urban&domain=" + d.location.hostname);
              i.style.width = "300px";
              i.style.height = "201px";
              i.style.border = "0";
              i.setAttribute("allowtransparency", "true");
              i.setAttribute("id", "widget-iframe");
              d.getElementById("metservice-widget").appendChild(i);
              })(document);
              </script>
              </div>
              
          </div>
    </div>
          <!-- /.row -->

          <!-- Contact Form -->
        <div class="row">
                <div class="col-md-6">
                    <div class="well-block">
                        <div class="well-title">
                            <h2>Questions? Book an Appointment</h2>
                    </div>
                    <form>
                            <!-- Form start -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="name">Name</label>
                                        <input id="name" name="name" type="text" placeholder="Name" class="form-control input-md">
                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="email">Email</label>
                                        <input id="email" name="email" type="text" placeholder="E-Mail" class="form-control input-md">
                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="date">Preferred Date</label>
                                        <input id="date" name="date" type="text" placeholder="Preferred Date" class="form-control input-md">
                                    </div>
                                </div>
                                <!-- Select Basic -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="time">Preferred Time</label>
                                        <select id="time" name="time" class="form-control">
                                            <option value="8:00 to 9:00">8:00 to 9:00</option>
                                            <option value="9:00 to 10:00">9:00 to 10:00</option>
                                            <option value="10:00 to 11:00">10:00 to 11:00</option>
                                            <option value="1:00 to 2:00">1:00 to 2:00</option>
                                            <option value="2:00 to 3:00">2:00 to 3:00</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Select Basic -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="appointmentfor">Appointment For</label>
                                        <select id="appointmentfor" name="appointmentfor" class="form-control">
                                            <option value="Service#1">Custom Makeover</option>
                                            <option value="Service#2">Consultation</option>
                                            <option value="Service#3">Beauty Lessons</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Button -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-primary" id="sendMessageButton">Make an Appointment</button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well-block">
                        <div class="well-title">
                            <h2>Why Appointment with Us</h2>
                        </div>
                        <div class="feature-block">
                            <div class="feature feature-blurb-text">
                                <h4 class="feature-title">24/7 Hours Available</h4>
                                <div class="feature-content">
                                    <p>Integer nec nisi sed mi hendrerit mattis. Vestibulum mi nunc, ultricies quis vehicula et, iaculis in magnestibulum.</p>
                                </div>
                        </div>
                        <div class="feature feature-blurb-text">
                                <h4 class="feature-title">Experienced Staff Available</h4>
                                <div class="feature-content">
                                    <p>Aliquam sit amet mi eu libero fermentum bibendum pulvinar a turpis. Vestibulum quis feugiat risus. </p>
                                </div>
                        </div>
                        <div class="feature feature-blurb-text">
                                <h4 class="feature-title">Low Price & Fees</h4>
                                <div class="feature-content">
                                    <p>Praesent eu sollicitudin nunc. Cras malesuada vel nisi consequat pretium. Integer auctor elementum nulla suscipit in.</p>
                                </div>
                        </div>
                        </div>
                </div>
            </div>
            <div class="col-lg-8 mb-4">
              <h3>Contact Us</h3>
              <form name="sentMessage" id="contactForm" novalidate>
                <div class="control-group form-group">
                  <div class="controls">
                    <label>Full Name:</label>
                    <input type="text" placeholder="Enter full name" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                    <p class="help-block"></p>
                  </div>
                </div>
                <div class="control-group form-group">
                  <div class="controls">
                    <label>Phone Number:</label>
                    <input type="tel" placeholder="Enter phone number" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
                  </div>
                </div>
                <div class="control-group form-group">
                  <div class="controls">
                    <label>Email Address:</label>
                    <input type="email" placeholder="Enter email address" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label" for="subject">Subject</label>
                    <select id="subject" name="subject" class="form-control">
                        <option value="Give Feedback">Give Feedback</option>
                        <option value="General Inquiry">General Inquiry</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="control-group form-group">
                  <div class="controls">
                    <label>Message:</label>
                    <textarea rows="10" cols="100" placeholder="Enter your message" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                  </div>
                </div>
                <div id="success"></div>
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-primary" id="sendMessageButton">Send Message</button>
              </form>
            </div>

          </div>
          <!-- /.row -->
          <h1 class="mt-4 mb-3">FAQ - 
          <small>Frequently Asked Questions</small>
          </h1>

          <div class="mb-4" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Frequently Asked Question 1</a>
                </h5>
              </div>

              <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-body">
                  <i> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid?</i> <br> 
                  3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Frequently Asked Question 2</a>
                </h5>
              </div>
              <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="card-body">
                <i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis metus aliquet, accumsan est ut, rutrum dui.</i> <br>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Frequently Asked Question 3</a>
                </h5>
              </div>
              <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-body">
                </div>
                <i>Proin nec interdum leo. Ut id varius leo. Maecenas et lorem dolor. Etiam malesuada dui laoreet, pharetra sem nec, sodales dui?</i> <br>
                Sed consequat efficitur imperdiet. Praesent erat ante, elementum eget risus in, pellentesque ultricies mauris. Nam aliquam efficitur odio, ut rutrum tellus volutpat ut.
              </div>
            </div>
            <div class="card">
              <div class="card-header" role="tab" id="headingFour">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Frequently Asked Question 4</a>
                </h5>
              </div>
              <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                <div class="card-body">
                <i>Donec maximus gravida lorem, nec tincidunt libero iaculis vel. Vivamus gravida a mauris sed vehicula. Duis et felis nibh?</i> <br>
                Maecenas urna mauris, laoreet et malesuada quis, euismod eget tellus. Donec vulputate semper elit, quis porta sem accumsan vitae. Proin varius sapien a vehicula convallis. Sed molestie ligula dolor. Curabitur sed leo et enim vulputate scelerisque. In hac habitasse platea dictumst. Curabitur commodo, nisl a scelerisque luctus, nibh orci facilisis neque, in varius nisi orci eget mi. Aenean varius pellentesque ornare. Sed mollis mattis arcu id volutpat. Aliquam eu pulvinar leo. 
                </div>
              </div>
            </div>
          </div>

      </div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ethereal Beauty 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Contact form JavaScript -->
    <!-- Do not edit these files! In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

  </body>

</html>
